package com.example.navbar

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.navbar.fragments.FavoritesFragment
import com.example.navbar.fragments.HomeFragment
import com.example.navbar.fragments.SettingsFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val homeFragment = HomeFragment()
        val favoritesFragment = FavoritesFragment()
        val settingsFragment = SettingsFragment()

        makeCurrentFragment(homeFragment)





    }

    private fun makeCurrentFragment(fragment: HomeFragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, android.support.v4.app.Fragment())
            commit()
        }

    }

private fun Boolean.setOnNavigationItemSelecterListener(function: () -> Boolean) {

}


